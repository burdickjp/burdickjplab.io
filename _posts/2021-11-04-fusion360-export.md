---
layout: post
title: "Exporting and Archiving from AutoDesk Fusion 360"
date: 2021-11-04
author: Jeffrey P Burdick
categories:
excerpt: We talk about the methods and file formats for getting data out of Fusion 360.
---

## Introduction 

While [AutoDesk Fusion 360](https://www.autodesk.com/products/fusion-360/overview) is an amazing collaborative design tool, some projects require archiving or exporting data. In this post we'll be going through methods of exporting data out of Fusion 360 and the advantages and disadvantages of different data formats.

## Exporting data

AutoDesk Fusion 360 can export data in several formats. While some preserve assemblies and relationships better, others are more useful for certain tasks such as 3D printing. We'll be going through data formats in descending order of data preservation from Fusion 360s native archive file, through STEP, IGES, and finally 3MF.

Fusion 360 archive, STEP, IGES, 3MF

### Fusion 360 archive

AutoDesk provides [a detailed walkthrough](https://knowledge.autodesk.com/support/fusion-360/troubleshooting/caas/sfdcarticles/sfdcarticles/How-to-make-a-local-archive-back-up-file-in-Fusion-360.html) for exporting an archive of a project, including screenshots. This file can only be imported back into Fusion 360. This brings in the entire project, with model history, toolpaths, links, etc.

While Fusion 360 archives preserve the project in its entirety, they are not readable by any other programs.

Exporting a STEP or IGES file follows the same procedure as the Fusion 360 archive by selecting either of those file formats in the drop-down. 

### STEP

[ISO 10303](https://en.wikipedia.org/wiki/ISO_10303) defines the STEP interchange format for CAD data. It can be read by many CAD tools. While this definition and interchange is not perfect, it does the best job of preserving solid model bodes and assemblies. [Boundary Representation](https://en.wikipedia.org/wiki/Boundary_representation) of [NURBS](https://en.wikipedia.org/wiki/Non-uniform_rational_B-spline) bodies is preserved. 

### IGES

[NIST](https://en.wikipedia.org/wiki/National_Institute_of_Standards_and_Technology) defines the [IGES](https://en.wikipedia.org/wiki/IGES) file format. It is an older format, originating with 2 dimensional drawings, but has been expanded to include 3 dimensional data. It is interesting in being [a recursive standard](https://en.wikipedia.org/wiki/IGES#A_recursive_standard).

IGES files maintain NURBS surfaces, but generally do not incorporate these into BREP solids. As such, while all the surfaces of a body are defined, their relationships are not.

### 3MF

[3D Manufacturing Format](https://en.wikipedia.org/wiki/3D_Manufacturing_Format) is designed to be a replacement for the archaic [STL format](https://en.wikipedia.org/wiki/STL_(file_format)). This format is predominantly used for [3D printing](https://en.wikipedia.org/wiki/3D_printing). This file format converts 3D geometry into a mesh of triangles. This is a [lossy conversion](https://en.wikipedia.org/wiki/Lossy_compression).

Exporting a [3MF](https://en.wikipedia.org/wiki/3D_Manufacturing_Format) file is different from the other formats. This option lives under the `make` option in the `tools` ribbon.

![](/assets/2021-11-04/fusion360_make.png)

This brings up the 3D printing utility dialog. Select the body you want to export. Below that, select `3MF`. While STl is a popular file format, it is important to mention that the STL file format has many issues. 3MF corrects many of these issues. It is a superior file format. Under `output` uncheck `Send to 3D Print Utility`. When you click `OK` an export dialog will open. 

![](/assets/2021-11-04/fusion360_3DprintUtility.png)

## Conclusion

For maximum compatibility, it is worth exporting all four of these options and saving them in a [git](https://en.wikipedia.org/wiki/Git) repository on [gitlab](https://gitlab.com/) or [github](https://github.com/).
