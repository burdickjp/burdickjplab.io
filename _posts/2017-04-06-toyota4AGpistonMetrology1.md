---
layout: post
title: "Toyota 4A-G piston metrology part 1"
date: 2017-04-06
author: Jeffrey P Burdick
categories:
excerpt: A quick description of some metrology I recently did on some 4A-G pistons.
---
# 4A-G pistons
I recently did some metrology on some 4A-G pistons. This

I have an 81.5 mm OEM AE111 piston and an 81 mm OEM AE101 piston.
![20v pistons](/assets/2017-04-06/pistons20v.jpg)

# laser scanning on CMM
First thing I did was [scan][] the pistons using a laser scanning head on a Mitutoyo CMM.
![AE101 piston on the CMM](/assets/2017-04-06/laserAE101.jpg)
Getting good results with [laser scanning][] or [photogrammetry][] and metal parts usually requires the use of a [powder][] to reduce reflections which would otherwise cause problems. Even so, I normally try without first, just to see how bad it will be. These parts went very well without, so I scanned them as you see them here.
This created a [point cloud][] which I turned into an [STL][] [polygon mesh][].

## point cloud
* [AE101](https://github.com/burdickjp/4AGpistons/raw/master/AE101.xyz)
* [AE111](https://github.com/burdickjp/4AGpistons/raw/master/AE111.xyz)

## mesh
* [AE101](https://github.com/burdickjp/4AGpistons/raw/master/AE101.stl)
* [AE111](https://github.com/burdickjp/4AGpistons/raw/master/AE111.stl)

## contour tracing
Next I took each piston and did some quick contact contour measurements using a Mitutoyo contracer. This provides a series of closely-spaced points. The stylus has a 25 um radius. This is about as accurate of a measurement as you can get over the surface of the piston.
![AE111 piston on contour tracer](/assets/2017-04-06/contourAE111-001.jpg)

I did one line through the exhaust pocket on the AE111 piston.
![AE111 piston on contour tracer at the end of tracing](/assets/2017-04-06/contourAE111-002.jpg)

On the AE101 piston I did a line through the exhaust pocket and through the center intake valve pocket.

I took pictures of the output screen on the software as well. They're not very useful, but they're better than nothing.

AE111
![screenshot from AE111 contour tracer scan](/assets/2017-04-06/screenshotContourAE111.jpg)

AE101 exhaust
![screenshot from AE101 exhaust contour tracer scan](/assets/2017-04-06/screenshotContourAE101e.jpg)

AE101 center intake
![screenshot from AE101 center intake contour tracer scan](/assets/2017-04-06/screenshotContourAE101c.jpg)

# future work
My next intention is to do touch probe measurements to be able to reconstruct the piston tops in a CAD package.

[point cloud]: https://en.wikipedia.org/wiki/Point_cloud
[STL]: https://en.wikipedia.org/wiki/STL_(file_format)
[polygon mesh]: https://en.wikipedia.org/wiki/Polygon_mesh
[laser scanning]: https://en.wikipedia.org/wiki/Laser_scanning
[photogrammetry]: https://en.wikipedia.org/wiki/Photogrammetry
[scan]: https://en.wikipedia.org/wiki/3D_scanner
[powder]: https://3dscanspray.com/
