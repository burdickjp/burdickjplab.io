---
layout: post
title: "Making a Quorn Mk3 Tool & Cutter Grinder part 2"
date: 2019-11-02 
author: Jeffrey P Burdick
categories:
excerpt: First operations on the base castings. 
---

# Introduction 

[Hemingway Kits' Quorn Mk3 kit](http://www.hemingwaykits.com/acatalog/Quorn__Mk3__T_C__Grinder.html) comes with drawings and construction notes. These notes detail operation setups with good pictures and pointers. I was curious if [Professor D.H. Chaddock](http://www.modelenginenews.org/meng/quorn/dhc.html)'s instructions were any different, so I picked up ["Quorn" Universal Tool and Cutter Grinder](https://www.amazon.com/dp/0905100913/). Thus far it seems to go into more detail on the thought process behind the design and construction but doesn't have much more to say about the setups.

# Getting started

Both Prof Chaddock and Hemingway Kits start the construction process with the base. Since my manual lathe is currently sitting in a POD in a warehouse I decided to start with milling operations on the base castings. I ordered up some [16 mm diameter tight-tolerance oil-hardening O1 tool steel rod](https://www.mcmaster.com/88625k76) from [McMaster-Carr](https://en.wikipedia.org/wiki/McMaster-Carr). I was looking for the hardest tight-tolerance rod I could get my hands on. I seem to have read wrong, as this comes at [Rockwell B90](https://en.wikipedia.org/wiki/Rockwell_scale) and McMaster has other material which is harder. Even so, it seemed to work just fine for what I was asking of it.

Along with my steel rod I ordered some [PVC sheet](https://www.mcmaster.com/87025k118) to help with compliance in clamping the castings in a vise. I had considered some copper sheet. Hemingway Kits suggests PVC and I hadn't tried it before, so I grabbed it out of curiousity. What I ordered seems a bit hard for the task, but still worked.

# Facing the feet

The first operation in Hemingway Kits instructions is to face the bottom of the castings while they are hanging from pins. Important to this process is ensuring that when the vise is closed that the casting is still hanging properly from the pins. In this way the feet and bore orientation is maintained. This is where the PVC sheet comes in. I cut two pieces so that they were just shorter than my vise jaws. I initially tried to set this whole thing up on parallels, but instead decided that the vise jaw faces were parallel enough for this roughing operation. I've shown my setup on the right side casting in the picture below.

![](/assets/2019-11-02/DSC_0189.JPG)

This setup took quite a lot of rolling the castings back and forth on the pins to get them to sit flat, clamping, wiggling, and hammering to get to a state that I felt was adequate for cutting.

I did some quick cutting parameters math and dropped in a [50 mm Glacern face mill](https://glacern.com/fm90) with [Mitsubishi F7030 inserts](https://www.amazon.com/dp/B01EKFSKX4/). I jogged and faced until I'd a decent cleanup on each casting.

At this point I deviated from Hemingway Kits directions. They are rather insistant that the two castings are cleaned up at the same height to establish the bore to feet relationship. I have a machine big enough to do some facing operations after I assemble the castings onto the shafts. As such, I decided to just cut enough to establish a datum surface.

![](/assets/2019-11-02/DSC_0188.JPG)


After cleanup I took the parts to the surface plate. Even with all this trying I found that my left side casting ended up slightly out of square. I decided that it was sufficiently square to continue on to the next operation: roughing in the shaft bores.

![](/assets/2019-11-02/DSC_0220.JPG)
