---
layout: post
title: "decentralizing my cloud part 1"
date: 2015-09-07
author: Jeffrey P Burdick
categories: 
excerpt: Two use-cases are introduced which provide novel solutions to the previously centralized cloud syncing services for notes and ToDo lists. Both cases use flat files following standard formats which are software and sync service agnostic.
---
# Abstract
Two use-cases are introduced which provide novel solutions to the previously centralized cloud syncing services for notes and ToDo lists. Both cases use flat files following standard formats which are software and sync service agnostic.

# Introduction  
In the past, I've used [Google Keep][], [Microsoft OneNote][], and for a short period of time [Evernote][] to assemble and share todo lists and notes. At school I encouraged doing group projects using [Google Drive][] or [OneDrive][].
A bit over a year ago I became tired of being the product of cloud services rather than the customer. I made the decision to remove myself, as much as possible, from systems which profited from my metadata. I started by removing things I could do without: I deleted my Facebook account and my Google+ accout. Satisfied that I did a good thing, I then replaced things which were easy: I switched from [Chromium][] to [Firefox][], and from using [Google search][] to [DuckDuckGo][]. Slowly and irregularly I've been tackling the harder problems.
[Bittorrent Sync][] came across my radar when it was in beta. This was novel by using a [peer-to-peer method][] of file syncronization. I stopped using Google Drive and OneDrive and started syncing my files with BTSync. With most software I opt for the open source alternative, and not very much later [Syncthing][] proved itself as a viable decentralized file syncing service. You can read about how Syncthing works [here](howSyncthingWorks). I've since built myself a NAS running [FreeNAS][] and Syncthing. It's always on, so syncing has become very easy.

I've since been looking for ways Syncthing can be leveraged to replace other centralized services.

# Notes

## Introduction  
I've used almost all of the afore-mentioned centralized cloud services for making notes. [Tomboy][] and [Tomdroid][] were a first start at replacing cloud services, but Tomdroid did not play well with the files Syncthing adds to a folder it is syncing.

## Implementation  
I then decided a flat text editor would work just as well, as long as it could browse folders well. I ended up stumbling upon [Writeily Pro][] recently, which is also capable of markdown syntax. I sync a folder for notes between my systems. On my phone I use Writeily Pro and on my desktops I use Kate or Gedit, depending on which system I'm on. It has thus far worked very well.

## The Bad  
I'd had some notes previously on OneNote with tables. Markdown does not do tables well, and ASCII tables are quite a pain on a mobile device. Other than that markdown does a good job of providing lists, titles, etc.

# Todo  

## Introduction  
I now use a tool called [todo.txt][]. Like many people had, I was introduced to the system through [a series of Lifehacker articles][].

## Implementation  
I use the bash scripts on my desktop system and [Simpletask Cloudless][] on my phone. I sync these using a specific Syncthing folder. Setting the refresh interval low provides a rather seamless transition from phone to desktop. I haven't yet tried this, but I think it'd work to have a second shared folder on my devices which would sync with other devices in my family. This is yet another folder to have to configure, but I could then have a Todo list which I share with my family and a ToDo list which I do not.

## The Good
the todo.txt syntax is pretty cool and the way it is implemented is very well thought out. Having a standard to adhere to makes some decisions easy and the standard seems rather painless to me thus far.

## The Bad  
Simpletask Cloudless won't parse several files and display the results. So if I have a ToDo list for myself, and a ToDo list for my family, and on each of those I have groceries it does not seem like I'd be able to see my entire groceries list together. I'd have to look at one, and then the other. While disadvantageous, it definitely doesn't detract much from the usefulness of it as it is.

# Future Development  
Since I've started using Syncthing to ... sync things ... I've thought of several places where it could prove useful. I'd like to have my contacts and calendars sync using Syncthing and my Firefox data such as bookmarks, passwords, history, etc. I think either of them would be easy to implement. Developers no longer need to worry about API, licensing, or debugging their syncing methods, they just need to provide a folder to point your syncing service at and follow a standard format for their data.

[Others have suggested implementing an OwnCloud instance to allow web access to services][]. Owncloud even has notes and todo plugins for its cloud services. I've played with OwnCloud some, but other than physically hosting it I feel it doesn't fit the decentralized bill. FreeNAS does have an owncloud plugin. I may play with that in the future.

# Conclusion
While setting up Syncthing and a FreeNAS instance takes quite a bit of time I don't feel that my everyday interaction with services has become much more difficult than it was using Google or Microsoft products. As a plus, by using standard formats for the services they become somewhat application agnostic. I can open up my todo.txt in an ever growing list of software, or even a plain text editor. With the use of regular files the implementation also becomes syncing servic agnostic. A user could just as easily use BTSync or dropbox for this if they so wished.

[Google Keep]: https://en.wikipedia.org/wiki/Google_Keep
[Microsoft OneNote]: https://en.wikipedia.org/wiki/Microsoft_OneNote
[Evernote]: https://en.wikipedia.org/wiki/Evernote
[Google Drive]: https://en.wikipedia.org/wiki/Google_Drive
[OneDrive]: https://en.wikipedia.org/wiki/OneDrive
[Chromium]: https://en.wikipedia.org/wiki/Chromium_%28web_browser%29
[Firefox]: https://en.wikipedia.org/wiki/Firefox
[Google search]: https://en.wikipedia.org/wiki/Google_Search
[DuckDuckGo]: https://en.wikipedia.org/wiki/DuckDuckGo
[Bittorrent Sync]: https://en.wikipedia.org/wiki/BitTorrent_Sync
[peer-to-peer method]: https://en.wikipedia.org/wiki/Peer-to-peer
[Syncthing]: https://en.wikipedia.org/wiki/Syncthing
[howSyncthingWorks]: https://github.com/syncthing/specs/blob/master/BEPv1.md
[FreeNAS]: http://www.freenas.org/
[tomboy]: https://en.wikipedia.org/wiki/Tomboy_%28software%29
[tomdroid]: https://launchpad.net/tomdroid
[Writeily Pro]: http://writeily.me/
[todo.txt]: http://todotxt.com/
[a series of Lifehacker articles]: http://lifehacker.com/tag/todotxt
[Simpletask Cloudless]: https://github.com/mpcjanssen/simpletask-android/blob/HEAD/src/main/assets/index.en.md
[Others have suggested implementing an OwnCloud instance to allow web access to services]: http://blog.bittorrent.com/2013/05/23/how-i-created-my-own-personal-cloud-using-bittorrent-sync-owncloud-and-raspberry-pi/