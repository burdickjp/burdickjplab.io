---
layout: post
title:  "Sharing a Jupyter Notebook using Binder"
date:   2019-05-26 16:55
author: Jeffrey P Burdick
categories: 
excerpt: How Python, Jupyter, and Binder can be used to share projects using interactive scientific notebooks.
---

# Introduction

A recent project of mine has created the need for a [gear ratio](https://en.wikipedia.org/wiki/Transmission_(mechanics)#Multi-ratio_systems) calculation tool. I decided it'd be a good opportunity to demonstrate the use of [Python](https://www.python.org/) in [Jupyter](https://jupyter.org/) for [scientific notebooks](https://en.wikipedia.org/wiki/Notebook_interface) and sharing those notebooks for interaction with [Binder](https://mybinder.org/).

## Python

When I was doing undergraduate studies in mechanical engineering many of the courses had lab experiments or homework which required calculations, interpretation, and visualization of data. Instructors and students used [Microsoft Excel](https://en.wikipedia.org/wiki/Microsoft_Excel) for much of the work. I've never liked the [spreadsheet](https://en.wikipedia.org/wiki/Spreadsheet) paradigm. I find the inclusion of data, calculations, formatting, and visualization in the same interface to be distracting. Given the opportunity, I'm prone to ranting about the [problems of the spreadsheet paradigm](https://en.wikipedia.org/wiki/Spreadsheet#Shortcomings) in less than kind words.
For more complex work, professors advocated for [MATLAB](https://en.wikipedia.org/wiki/MATLAB). While this wasn't difficult for academic use, I was worried about learning a tool that would be encumbered with licenses in a professional context.  
I did some investigating and eventually started using [SageMath](http://www.sagemath.org/) for my lab calculations. After I graduated and started working professionally I moved to using [Anaconda](https://www.anaconda.com/) for setting up and managing a regular Python environment.

## Jupyter Notebooks

While Python is a powerful language, I find sharing to be best done in [notebooks](https://ipython.org/notebook.html). They allow the developer to concentrate on code, then encorporate documentation, formatting, and visualization without having one distract from the other. Regular version control tools like [Git](https://git-scm.com/) can be used. These advantages supercede a lot of the advantages of spreadsheets to me.

## Binder

Once a notebook is in a git repository, Binder can be used to share notebooks in an interactive way. Binder works by starting a [container](https://en.wikipedia.org/wiki/OS-level_virtualisation) with a defined [environment](https://docs.python.org/3/tutorial/venv.html). The specified repository is cloned into it and then a Jupyter interface is presented to the user.

# Our example

[My git repository](https://gitlab.com/burdickjp/gearRatioCalculator) contains datasets stored as [JSON](https://json.org/) files. A Python 3 notebook loads all of the files and produces a [Bokeh](https://bokeh.pydata.org/en/latest/) plot. 

# Conclusion

I feel that it is important to grow [End-user development](https://en.wikipedia.org/wiki/End-user_development) toward programming tools which are scalable, maintainable, and encourage modern coding practices and processes. I hope this small tool demonstrates some of the advantages of using Jupyter and Bokeh for these purposes.
