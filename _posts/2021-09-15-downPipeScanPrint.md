---
layout: post
title: "Scanning, engineering, and printing a downpipe"
date: 2021-09-15
author: Jeffrey P Burdick
categories:
excerpt: Developing a downpipe for the Suzuki Cappuccino
---

## Introduction 

Here a workflow is presented for developing and test fitting a replacement [downpipe](https://en.wikipedia.org/wiki/Exhaust_system#Catalytic_converter) for the [Suzuki Cappuccino](https://en.wikipedia.org/wiki/Suzuki_Cappuccino) using [flat bad scanning](https://en.wikipedia.org/wiki/Flatbed_scanner), [a portable measuring arm](https://en.wikipedia.org/wiki/Coordinate_measuring_machine#Portable_coordinate-measuring_machines), [AutoDesk Fusion 360](https://www.autodesk.com/products/fusion-360/overview), and an [Ultimaker 3X](https://ultimaker.com/3d-printers/ultimaker-3) [3D printer](https://en.wikipedia.org/wiki/Fused_deposition_modeling).

## The part

The Suzuki Cappuccino uses a large catalytic converter located in the down pipe directly off of the turbocharger. The part uses cast iron construction and on many cars are a restriction to exhaust flow, limiting performance. For this project we're developing a replacement part with the intention to perform better.

![](/assets/2021-09-15/141.png)

## Measuring

The replacement downpipe needs to mate to the turbocharger and the rest of the exhaust system and fit within the bounds of the OEM downpipe. 

To engineer a new part the old part must be measured. Two methods of measurement were implemented in this project. First the mating surfaces and gaskets of the part were scanned on a flat bed scanner. Second, the entire part was scanned using a Hexagon Absolute Arm.

### Flat bed scanning

The reverse engineering process started with scanning the mating flanges on a [flat bed image scanner](https://en.wikipedia.org/wiki/Image_scanner) with a pair of machinists' scales. these images were brought into AutoDesk Fusion 360 and scaled so that the distances on the scales are correct. Measurements could then be taken from the scans and modeled so that geometric features can be created in a sketch.

![](/assets/2021-09-15/flatScan.jpg)

Since we are not making a mating part to this flange, care must be taken to model in the correct direction from the scanned image. This method of scanning and measuring provides very accurate measurements for flat parts.

![](/assets/2021-09-15/flange.png)

### Scanning arm

To produce the bounds of the OEM downpipe, it was scanned using a [Romer arm](https://en.wikipedia.org/wiki/Romer_arm). This produced a [mesh](https://en.wikipedia.org/wiki/Polygon_mesh) which was exported as an [STL](https://en.wikipedia.org/wiki/STL_(file_format)) and imported into AutoDesk Fusion 360.

![](/assets/2021-09-15/scan.png)

## Modeling

With the flat-bed images and the 3D scan in the same model, the flanges can be oriented in space with respect to the mesh. 

![](/assets/2021-09-15/flangesInSpace.png)

Three-dimensional features can then be modeled to connect the two flanges and checked that the new design fits within the bounds of the original part.

![](/assets/2021-09-15/modelAndScan.png)


### Turbine Outlet Elbow

One of the most difficult aspects of this project has been modeling a turbine outlet elbow which flows and looks well, is manufacturable, and is economical. Several manufacturing methods could be used for making a part like this: [sand casting](https://en.wikipedia.org/wiki/Sand_casting), [investment casting](https://en.wikipedia.org/wiki/Investment_casting), [laser powder bed fusion additive manufacturing](https://en.wikipedia.org/wiki/Laser_powder_bed_fusion), [MIM post-processed](https://en.wikipedia.org/wiki/Metal_injection_molding#Process) [additively manufactured green parts](https://en.wikipedia.org/wiki/Desktop_Metal#Studio_System), [5 axis CNC machining](https://en.wikipedia.org/wiki/Multiaxis_machining), and [3 axis CNC machining](https://en.wikipedia.org/wiki/Numerical_control).

Of those options, 3 axis CNC machining would be preferred, as it can be accomplished in-house. It is, however, the most difficult to make flow correctly and look good in this envelope.

![](/assets/2021-09-15/3AxisTurbineOutelt.png)

This part is going to be a weldment, so some standard bends were modeled and Fusion 360's joint tools were used to orient them with respect to eachouther This allows the machined elbow and pieces of the weldment and individual joints to be manipulated with the other joints adjusting in real time.

![](/assets/2021-09-15/joints.png)

## Printing

A fit test prototype was [3D printed](https://en.wikipedia.org/wiki/3D_printing) using an [Ultimaker 3 Extended](https://github.com/Ultimaker/Ultimaker3Extended) using [Proto-Pasta back-to-basics natural PLA](https://www.proto-pasta.com/collections/all/products/back-to-basics-natural-pla). The extra Z height of the Ultimaker 3 Extended proved vital for printing this part in one piece.

![](/assets/2021-09-15/curaScreenshot.png)

## Next steps

A 3D print can be used to check fitment and clearance. After the design is finalized the elbow will be machined and a welding fixture will be made. Then weldments will be produced and tested.
