---
layout: post
title: "decentralizing my cloud part 2"
date: 2015-09-13
author: Jeffrey P Burdick
categories: 
excerpt: A method of using randomized password security is presented using ZX2C4's pass and Syncthing.
---
## Abstract
A method of using randomized password security is presented using ZX2C4's pass and Syncthing.

## Introduction
I've previously been rather lazy with my passwords. In discussing this series of articles with a friend he suggested I look into password management and syncing. I looked at several open source solutions including [keepass][] before landing on [pass][]. While pass is designed to work with a git server the point of this series of articles is syncing between devices using peer-to-peer software. If you've got a git server you trust then that would remove quite a bit of complication.

## Implementation
In order to get pass to work across my devices I had to start by implementing the same [PGP][] key across my devices. Starting PGP on a Linux host is covered rather concisely [on Archlinux's wiki here][gnupg]. PGP keys can be exported and imported, but the more secure way is to [use SSH as detailed here][pgpssh]. PGP can be implemented on android using [Openkeychain][].
Once you've got PGP up on your devices start pass on one. Sync the folder to your other devices using the method of your choice. I like [Syncthing][]. Tada! you've got your passwords secure on all your devices.

## The Bad
The android client seems to be rather slow decrypting passwords. I ran into some trouble getting my PGP keys exported and imported correctly. It wasn't insurmountable with a quick search of some documentation. Keepass would've probably been easier to set up and probably easier to use. I chose pass over keepass because I feel pass more closely follows the Unix philosophy.

## Future Development
There is a Firefox plugin in development. That looks good. It'd be really cool if it worked on android.  
What would be really cool is if there'd be a way to make [kwallet][] or [GNOME Keyring][] use pass as their database.

## Conclusion
I've been slowly having pass generate passwords for websites as I use them. Syncing to my other devices seems to be very quick.

[keepass]: https://en.wikipedia.org/wiki/KeePass
[pass]: http://www.passwordstore.org/
[pgp]: https://en.wikipedia.org/wiki/Pretty_Good_Privacy
[gnupg]: https://wiki.archlinux.org/index.php/GnuPG#Create_key
[pgpssh]: http://www.debuntu.org/how-to-importexport-gpg-key-pair/
[Openkeychain]: http://www.openkeychain.org/about/
[syncthing]: https://syncthing.net/
[kwallet]: https://en.wikipedia.org/wiki/KWallet
[gnome keyring]: https://en.wikipedia.org/wiki/GNOME_Keyring