---
layout: post
title:  "using Git for tuning"
date:   2015-09-16 20:40
author: Jeffrey P Burdick
categories: 
excerpt: There are many tools and practices used in the development and maintenance of software which we could do well to learn and apply in working with engine management system tunes.
---
# Introduction
Developing and maintaining a performance tune is a lot like developing and maintaining software. There are many tools and practices used in the development and maintenance of software which we could do well to learn and apply in working with tunes.
Of those tools and practices, I'd like to introduce you to a concept called [revision control][]. Through revision control we're going to discuss repositories, the use of a bugs system, and the use of releases.

# Revision Control
Most people tuning are already implementing some kind of preleminary revision control. Most likely they make a copy of their files to so they can return to a previous state if things go wrong with the changes they are making or if they want to be able to return to a known good state between tuning sessions. Some software, such as TunerStudio, has built-in revision control, but then you're limited to having revision control for just your tuning files.
There's a standard set of tools, independent of the kind of work being done, which are used for revision control in the software development community. [Git][] is one such tool. It allows a user to save commits. These are snapshots of the contents of a folder at a given time. File additions or subtractions and changes are saved for a given state of the folder.
These changes can be branched and merged so a tuner can, for instance, start a branch focusing on O2 tables and save the states and then load an unmodified state and start a second branch focusing on cold start ignition. They can be maintained separately and refined as needed. When one is done its changes can be brought back into the main branch and also brought into the other. When both are done the "known good state" can now be pointed at the state which has both changes made. This all occurs within the same folder without the need of the tuner to keep track of anything beyond glancing at what branch they are on.
[You'll want to watch this video introducing version control][revisioncontrolvid]. If you don't like that particular video, or are still confused on WTF I'm talking about, do a quick search on your own and read up on revision control, or version control, or versioning systems. It's a very steep rabbit hole to tumble down, but we don't necessarily need all of the features that Git offers.

# Repositories
Repositories allow multiple people to contribute to the same project. Contributors can check out branches, make changes, and signal to the maintainer that their branch is ready to merged. Others can see the changes and the contributor's notes and be able to pull them into their own branches easily without overwriting what they are working on.
To parallel our above example, I can be maintaining a repo. Tuner A can check out the repo and be refining O2 tables. Tuner B can check out the repo and be refining cold start ignition. When tuner A is done they request I incorporate the changes into the main repo so everyone can benefit. I do so in a way which says these changes are not yet proven. Tuner B brings them into their repo to make sure they don't negatively affect their cold start modifications. Once they are done with cold start they request I pull in those changes as well. I merge them both in, and after some time without problems signal they are ready for regular use.

# Bug systems
Repository sited like [Gitlab](https://gitlab.com/) offer bug and feature tracking. This removes the need for the tuner to track those things themselves.

# Releases
Gitlab and similar sites like bitbucket and github allow developers to define specific releases. This provides users easy access to the content without wading through the complication of pulling an entire repository.

# Conclusion
I use git repos for everything from CAD files and CAM programs to configurations for 3d printing and (as this article suggests) tuning. [I have several open hardware repos up on Gitlab](https://gitlab.com/burdickjp). This website is even hosted on Gitlab. Being able to leverage a revision system has proven useful on many, many occasions and in many ways which would not be otherwise obvious. I encourage you to learn a revision system, such as git, and see where it becomes useful in your everyday life.

[revision control]: https://en.wikipedia.org/wiki/Version_control
[git]: https://en.wikipedia.org/wiki/Git_%28software%29
[revisioncontrolvid]: https://www.youtube.com/watch?v=dK4iCStqAxc
