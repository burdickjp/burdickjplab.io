---
layout: page
title: About
permalink: /about/
---

I am a maker by passion, additive manufacturing researcher by profession, mechanical engineer by education, and combat veteran by history. I advocate for open access research and open source hardware. I like Pho, Japanese cars, and Neal Stephenson novels. I enjoy getting my hands dirty with prototyping and rapid manufacturing. Contact me if you think I'm right for your project!

### Contact me

[burdickjp@protonmail.com](mailto:burdickjp@protonmail.com)
